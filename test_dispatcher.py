import unittest
from Valentyn_Kirianov import *

class TestDispatcher(unittest.TestCase):
	def setUp(self):
		self.employees = []
		self.tasks = []
		self.employees.append(Employee(name='First', task_point=11 ))
		self.employees.append(Employee(name='Second', task_point=15 ))
		self.tasks.append(Task(name='Task1', task_point=10))
		self.tasks.append(Task(name='Task2', task_point=6))
		self.tasks.append(Task(name='Task2', task_point=2))

	def test_dispatch(self):
		dispatcher = TaskDispatcher(self.employees)
		dispatcher.dispatch(self.tasks)
		first = None
		second = None
		for e in dispatcher.employees:
			if e.name =='First':
				first = e
			else:
				second = e
		self.assertEqual(first.workload, 8)
		self.assertEqual(second.workload, 10)

if __name__ == '__main__':
	unittest.main()