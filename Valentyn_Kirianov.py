import csv

class Task(object):
    '''Class represents task information'''

    def __init__(self, name, task_point=1):
        self.name = name
        self.task_point = int(task_point)
    
    def __str__(self):
        return 'Task: %s \n Complexity: %d' % ( self.name, self.task_point)


class Employee(object):
    '''Class represents employee information (including assigned tasks)'''

    def __init__(self, name, task_point):
        self.name = name
        self.ptp = int(task_point)
        self.workload = 0
        self.tasks = []

    @property
    def congestion(self):
        return float(self.workload) / self.ptp * 100 

    def assign_task(self, task, max_workload=None):
        if max_workload is None:
            max_workload = self.ptp

        if (task.task_point + self.workload) < max_workload:
            self.tasks.append(task)
            self.workload += task.task_point
            return True
        else:
            return False
           
    def __str__(self):
        return 'Employee: %s\nPTP: %d\nAssigned %d task(s), workload - %.1f%%' % (
		self.name, 
        self.ptp, 
		len(self.tasks),
		self.workload)


class TaskDispatcher(object):
    def __init__(self, employees):
        employees.sort(key=lambda x: x.ptp, reverse=True)    
        self.employees = employees
        self.workload = .0
        self.tasks = []
        self.tasks_workload = 0
        self.employees_workload = sum(x.ptp for x in self.employees)
        self.assigned_tasks = 0

    def dispatch(self, tasks):
        '''Assign tasks using FFD algorithm'''
        self.tasks+=tasks
        self.tasks.sort(key=lambda x: x.task_point, reverse=True)
        self.tasks_workload = sum(x.task_point for x in self.tasks)
        self.workload = float(self.tasks_workload) / self.employees_workload * 100
        if self.workload > 100:
            raise Exception('Tasks workload bigger than employees can do')
        for task in self.tasks:
            assigned = False
            for e in self.employees:
                max_workload = e.ptp * (self.workload / 100) 
                assigned = e.assign_task(task, max_workload)
                if assigned:
                    self.assigned_tasks += 1
                    break
            if not assigned:
                self.employees.sort(key=lambda x: x.congestion ) # sort employees by congestion (percents)
                for e in self.employees:
                    assigned = e.assign_task(task)
                    if assigned:
                        self.assigned_tasks+= 1
                        break


if __name__ == '__main__':
    employees = []
    tasks = []
    with open('data/emp.csv', 'r') as csv_file:
        reader = csv.DictReader(csv_file)
        for line in reader:
            employees.append(Employee(**line))

    with open('data/tsk.csv', 'r') as csv_file:
        reader = csv.DictReader(csv_file)
        for line in reader:
            tasks.append(Task(**line))

    dispatcher = TaskDispatcher(employees)
    dispatcher.dispatch(tasks)
    for e in dispatcher.employees:
        print '%s (Personal Task Point: %d Workload: %d)' % (e.name, e.ptp, e.workload )
        for tsk in e.tasks:
            print '\t%s (task_point: %d)' % (tsk.name, tsk.task_point)

